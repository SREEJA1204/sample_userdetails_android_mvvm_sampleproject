package com.alimuzaffar.testproject.ui.user;

import android.app.ListActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;

import com.alimuzaffar.testproject.R;
import com.alimuzaffar.testproject.databinding.ActivityLoginBinding;
import com.alimuzaffar.testproject.ui.data.DataSync;
import com.alimuzaffar.testproject.ui.listview.ListUserAdapter;
import com.alimuzaffar.testproject.ui.user.usermodel.UserFields;
import com.alimuzaffar.testproject.ui.user.usermodel.UserViewModel;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

public class UserActivity extends ListActivity {
    private UserViewModel userModel;
    private ListView listView = findViewById(R.layout.user_list);
    @Override
    public <T extends View> T findViewById(int id) {
        return super.findViewById(id);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setupBindings(savedInstanceState);
    }

    private void setupBindings(Bundle savedInstanceState) {
        ActivityLoginBinding activityBinding = DataBindingUtil.setContentView(this, R.layout.activity_user);
        userModel = ViewModelProviders.of(this).get(UserViewModel.class);
        if (savedInstanceState == null) {
            userModel.init();
        }

        String[] values =  getValuesFromJSON();
        activityBinding.setModel(userModel);
        ListUserAdapter adapter = new ListUserAdapter(this, values);
        listView.setAdapter(adapter);
    }

    private String[] getValuesFromJSON() {
     new DataSync().execute();
    }
}


package com.alimuzaffar.testproject.ui.photo.photomodel;

import androidx.databinding.BaseObservable;
import androidx.lifecycle.MutableLiveData;

public class PhotoForm extends BaseObservable {
    private PhotoFields fields = new PhotoFields();
    private MutableLiveData<PhotoFields> buttonClick = new MutableLiveData<>();

    public void onClick() {
        buttonClick.setValue(fields);
    }

    public MutableLiveData<PhotoFields> getPhotoFields() {
        return buttonClick;
    }

    public PhotoFields getFields() {
        return fields;
    }
}

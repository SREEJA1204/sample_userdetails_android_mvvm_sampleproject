package com.alimuzaffar.testproject.ui.user.usermodel;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class UserViewModel extends ViewModel {
    private UserForm user;

    public void init(){

    }
    public UserForm getUser() {
        return user;
    }

    public void onButtonClick() {
        user.onClick();
    }

    public MutableLiveData<UserFields> getUserFields() {
        return user.getUserFields();
    }

    public UserForm getForm() {
        return user;
    }

}

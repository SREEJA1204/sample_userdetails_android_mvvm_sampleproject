package com.alimuzaffar.testproject.ui.data;

import android.os.AsyncTask;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

public class DataSync extends AsyncTask<Integer, Integer, String> {


    @Override
    protected String doInBackground(Integer... params) {
//        for (int count = 0; count <= params[0]; count++) {
//            try {
//                Thread.sleep(1000);
//
//                publishProgress(count);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
//        }

        String res= "{\"param1\":\"hello\"}";
        JSONObject response = null;
        try {
            response = new JSONObject(res);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            response = new JSONParse().getJSONObjectFromURL("https://jsonplaceholder.typicode.com/users"); // calls method to get JSON object

        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return response.toString();
    }
    @Override
    protected void onPostExecute(String result) {
        System.out.println("result::::"+result);
        //progressBar.setVisibility(View.GONE);
    }
    @Override
    protected void onPreExecute() {
        //txt.setText("Task Starting...");
    }
    @Override
    protected void onProgressUpdate(Integer... values) {
        //txt.setText("Running..."+ values[0]);
        //progressBar.setProgress(values[0]);
    }
}
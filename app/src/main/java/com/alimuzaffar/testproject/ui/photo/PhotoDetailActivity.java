package com.alimuzaffar.testproject.ui.photo;

import android.os.Bundle;
import android.widget.Toast;

import com.alimuzaffar.testproject.R;
import com.alimuzaffar.testproject.databinding.ActivityLoginBinding;
import com.alimuzaffar.testproject.ui.photo.photomodel.PhotoFields;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

public class PhotoDetailActivity extends AppCompatActivity {
    private PhotoViewModel photoModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setupBindings(savedInstanceState);
    }

    private void setupBindings(Bundle savedInstanceState) {
        ActivityLoginBinding activityBinding = DataBindingUtil.setContentView(this, R.layout.actvity_photo);
        photoModel = ViewModelProviders.of(this).get(PhotoViewModel.class);
        if (savedInstanceState == null) {
            photoModel.init();
        }
        activityBinding.setModel(photoModel);
        setupButtonClick();
    }

    private void setupButtonClick() {
        photoModel.getPhotoFields().observe(this, new Observer<PhotoFields>() {
            @Override
            public void onChanged(PhotoFields photoFields) {
                Toast.makeText(PhotoDetailActivity.this,"",
                        // "Email " + userFields.getEmail() + ", Name " + userFields.getName(),
                        Toast.LENGTH_SHORT).show();
            }
        });
    }
}

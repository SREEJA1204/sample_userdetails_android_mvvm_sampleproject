package com.alimuzaffar.testproject.ui.user.usermodel;

import androidx.databinding.BaseObservable;
import androidx.lifecycle.MutableLiveData;

public class UserForm extends BaseObservable {
    private UserFields fields = new UserFields();
    private MutableLiveData<UserFields> buttonClick = new MutableLiveData<>();

    public void onClick() {
            buttonClick.setValue(fields);
    }

    public MutableLiveData<UserFields> getUserFields() {
        return buttonClick;
    }

    public UserFields getFields() {
        return fields;
    }
}

package com.alimuzaffar.testproject.ui.photo;

import com.alimuzaffar.testproject.ui.photo.photomodel.PhotoFields;
import com.alimuzaffar.testproject.ui.photo.photomodel.PhotoForm;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class PhotoViewModel extends ViewModel {
    private PhotoForm photo;

    public void init(){

    }
    public PhotoForm getPhoto() {
        return photo;
    }

    public void onButtonClick() {
        photo.onClick();
    }

    public MutableLiveData<PhotoFields> getPhotoFields() {
        return photo.getPhotoFields();
    }

    public PhotoForm getForm() {
        return photo;
    }
}
